#!/bin/bash

echo "Updating sub-repos..."

git submodule update --recursive --remote

echo "Done."